// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  weatherApiUrl: 'http://api.openweathermap.org/data/2.5/weather',
  weatherApiKey: '862953b00511518a6f61f0cbf2b8a066',
  cityApiUrl: 'https://api.geoapify.com/v1/geocode/autocomplete',
  cityApiKey: '2c0e0007d180424ab106113135318ee0'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

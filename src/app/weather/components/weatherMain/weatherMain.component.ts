import {Component, Input, OnInit} from '@angular/core';
import {WeatherResponseInterface} from '../../types/weatherResponse.interface';

@Component({
  selector: 'app-weather-main',
  templateUrl: './weatherMain.component.html',
  styleUrls: ['./weatherMain.component.scss']
})
export class WeatherMainComponent implements OnInit {
  @Input('weather') weatherProps: WeatherResponseInterface | null;


  constructor() {
  }

  ngOnInit(): void {
  }

  public get currentWeather() {
    return Math.round(this.weatherProps.main.temp);
  }

  public get weatherDescription(){
    return this.weatherProps.weather[0].description;
  }

  public get weatherIcon(){
    const iconName = this.weatherProps.weather[0].main.toLocaleLowerCase();
    return `/assets/styles/icons/${iconName}.png`
  }
}

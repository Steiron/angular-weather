import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-weather-unit',
  templateUrl: './weatherUnitToggler.component.html',
  styleUrls: ['./weatherUnitToggler.component.scss']
})
export class WeatherUnitTogglerComponent implements OnInit {

  @Input('selectedUnit') selectedUnitProps: string;
  @Output('onChangeUnit') onChangeUnitEmit = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onChaneUnit(unit: string): void {
    if (this.selectedUnitProps === unit) return;

    this.onChangeUnitEmit.emit(unit);
  }
}

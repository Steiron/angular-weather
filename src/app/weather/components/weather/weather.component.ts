import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';

import {isLoadingSelector, weatherSelector} from '../../store/selectors';
import {getWeatherAction} from '../../store/actions/getWeather.action';
import {GetWeatherRequestInterface} from '../../types/getWeatherRequest.interface';
import {WeatherResponseInterface} from '../../types/weatherResponse.interface';
import {CityCoordsInterface} from '../../../city/types/cityCoordsInterface';
import {cityCoordsSelector} from '../../../city/store/selectors';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, OnDestroy {
  isLoading$: Observable<boolean>;
  weather$: Observable<WeatherResponseInterface>;
  destroyed$: Subject<void> = new Subject();
  currentPosition$: Observable<CityCoordsInterface>;
  selectedUnit: string = 'metric';

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchWeather();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(null);
    this.destroyed$.complete();
  }

  private initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isLoadingSelector));
    this.weather$ = this.store.pipe(select(weatherSelector));
    this.currentPosition$ = this.store.pipe(select(cityCoordsSelector));
  }

  private fetchWeather(): void {
    this.currentPosition$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((coords: CityCoordsInterface) => {
        const getWeatherRequest: GetWeatherRequestInterface = {
          units: this.selectedUnit,
          lat: coords.latitude,
          lon: coords.longitude
        };
        this.store.dispatch(getWeatherAction({getWeatherRequest}));
      });
  }

  onChangeUnit(unit: string): void {
    this.selectedUnit = unit;
    this.fetchWeather();
  }
}

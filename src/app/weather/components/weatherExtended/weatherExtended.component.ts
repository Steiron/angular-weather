import {Component, Input} from '@angular/core';
import {WeatherResponseInterface} from '../../types/weatherResponse.interface';

@Component({
  selector: 'app-weather-extended',
  templateUrl: './weatherExtended.component.html',
  styleUrls: ['./weatherExtended.component.scss']
})
export class WeatherExtendedComponent {
  @Input('weather') weatherProps: WeatherResponseInterface | null;
  @Input('selectedUnit') selectedUnitProps: string;

  get windSpeed() {
    const speedUnit = this.selectedUnitProps === 'metric' ? 'м/с' : 'мили/ч';
    return `${this.weatherProps.wind.speed} ${speedUnit}`;
  }

  get rainRate() {
    return this.weatherProps.rain ? this.weatherProps.rain['1h'] * 100 : 0;
  }
}

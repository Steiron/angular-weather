import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {WeatherComponent} from './components/weather/weather.component';
import {WeatherMainComponent} from './components/weatherMain/weatherMain.component';
import {WeatherExtendedComponent} from './components/weatherExtended/weatherExtended.component';
import {CityModule} from '../city/city.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {reducer} from './store/reducer';
import {GetWeatherEffect} from './store/effects/getWeather.effect';
import {WeatherService} from './services/weather.service';
import {WeatherUnitTogglerComponent} from './components/weatherUnitToggler/weatherUnitToggler.component';
import {WindDirectionPipe} from './pipes/windDirection.pipe';

@NgModule({
  imports: [
    CommonModule,
    CityModule,
    StoreModule.forFeature('weather', reducer),
    EffectsModule.forFeature([GetWeatherEffect]),
  ],
  declarations: [
    WeatherComponent,
    WeatherMainComponent,
    WeatherExtendedComponent,
    WeatherUnitTogglerComponent,
    WindDirectionPipe,
  ],
  exports: [WeatherComponent],
  providers: [WeatherService],
})
export class WeatherModule {}

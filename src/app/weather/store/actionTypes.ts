export enum ActionType {
  GET_WEATHER = '[Get Weather] Get Weather',
  GET_WEATHER_SUCCESS = '[Get Weather] Get Weather success',
  GET_WEATHER_FAILURE = '[Get Weather] Get Weather failure',
}

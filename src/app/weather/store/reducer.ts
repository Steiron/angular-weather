import {Action, createReducer, on} from '@ngrx/store';

import {WeatherStateInterface} from '../types/weatherState.interface';
import {
  getWeatherAction,
  getWeatherFailureAction,
  getWeatherSuccessAction,
} from './actions/getWeather.action';

const initialState: WeatherStateInterface = {
  isLoading: false,
  data: null,
};

const getWeatherReducer = createReducer(
  initialState,
  on(
    getWeatherAction,
    (state): WeatherStateInterface => ({
      ...state,
      isLoading: true,
    })
  ),
  on(
    getWeatherSuccessAction,
    (state, action): WeatherStateInterface => ({
      ...state,
      isLoading: false,
      data: action.weather,
    })
  ),
  on(
    getWeatherFailureAction,
    (state): WeatherStateInterface => ({
      ...state,
      isLoading: false,
    })
  )
);

export function reducer(state: WeatherStateInterface, action: Action) {
  return getWeatherReducer(state, action);
}

import {createFeatureSelector, createSelector} from '@ngrx/store';
import {WeatherStateInterface} from '../types/weatherState.interface';
import {AppStateInterface} from '../../shared/types/appState.interface';

export const weatherFeatureSelector = createFeatureSelector<
  AppStateInterface,
  WeatherStateInterface
>('weather');

export const isLoadingSelector = createSelector(
  weatherFeatureSelector,
  (weatherState: WeatherStateInterface) => weatherState.isLoading
);

export const weatherSelector = createSelector(
  weatherFeatureSelector,
  (weatherState: WeatherStateInterface) => weatherState.data
);

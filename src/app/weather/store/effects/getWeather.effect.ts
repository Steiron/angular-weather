import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';

import {getWeatherAction, getWeatherFailureAction, getWeatherSuccessAction} from '../actions/getWeather.action';
import {WeatherService} from '../../services/weather.service';
import {WeatherResponseInterface} from '../../types/weatherResponse.interface';

@Injectable()
export class GetWeatherEffect {
  constructor(
    private actions$: Actions,
    private weatherService: WeatherService
  ) {
  }

  getWeather$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getWeatherAction),
      switchMap(({getWeatherRequest}) => {
        return this.weatherService.getWeather(getWeatherRequest).pipe(
          map((weather: WeatherResponseInterface) => {
            return getWeatherSuccessAction({weather});
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(getWeatherFailureAction());
          })
        );
      })
    )
  );

}

import {createAction, props} from '@ngrx/store';
import {ActionType} from '../actionTypes';
import {GetWeatherRequestInterface} from '../../types/getWeatherRequest.interface';
import {WeatherResponseInterface} from '../../types/weatherResponse.interface';

export const getWeatherAction = createAction(
  ActionType.GET_WEATHER,
  props<{getWeatherRequest: GetWeatherRequestInterface}>()
);
export const getWeatherSuccessAction = createAction(
  ActionType.GET_WEATHER_SUCCESS,
  props<{weather: WeatherResponseInterface}>()
);
export const getWeatherFailureAction = createAction(ActionType.GET_WEATHER_FAILURE);

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'windDirection'
})
export class WindDirectionPipe implements PipeTransform {
  const;
  directions: WindDirection = {
    'северный': [-11.25, 11.25],
    'северный, северо-восточный': [11.25, 33.75],
    'северо-восточный': [33.75, 56.25],
    'восточный, северо-восточный': [56.25, 78.75],
    'восточный': [78.75, 101.25],
    'восточный, юго-восточный': [101.25, 123.75],
    'юго-восточный':  [123.75, 146.25],
    'южно, юго-восточный': [146.25, 168.75],
    'южный': [168.75, 191.25],
    'южный,юго-западный': [191.25, 213.75],
    'юго-западный': [213.75, 236.25],
    'западный, юго-западный': [236.25, 258.75],
    'западный': [258.75, 281.25],
    'западный, северо-западный': [281.25, 303.75],
    'северо-западный': [303.75, 326.25],
    'северный, северо-западный':  [326.25, 348.75],
  };

  transform(directionValue: number, ...args): string {
    return Object.keys(this.directions).filter((direction: string) => {
      const directionValues = this.directions[direction];
      return directionValue >= directionValues[0] && directionValue <= directionValues[1];
    }).pop();
  }
}

interface WindDirection {
  [key: string]: number[];
}

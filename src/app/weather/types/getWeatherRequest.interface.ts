export interface GetWeatherRequestInterface {
  lat: number;
  lon: number;
  units: string;
}

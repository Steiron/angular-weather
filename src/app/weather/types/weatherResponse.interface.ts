export interface WeatherResponseInterface {
  weather: WeatherInterface[],
  main: WeatherMainInterface,
  wind: WeatherWindInterface,
  rain: WeatherRainInterface | null,
  coord: WeatherCoordsInterface,
  name: string
}


interface WeatherInterface {
  main: string,
  description: string
}

interface WeatherMainInterface {
  temp: number,
  pressure: number,
  humidity: number
}

interface WeatherWindInterface {
  speed: number,
  deg: number
}

interface WeatherRainInterface {
  '1h': number;
}

interface WeatherCoordsInterface {
  lat: number,
  lon: number
}

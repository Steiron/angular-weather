import {WeatherResponseInterface} from './weatherResponse.interface';

export interface WeatherStateInterface {
  isLoading: boolean,
  data: WeatherResponseInterface | null;
}

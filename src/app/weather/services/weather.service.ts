import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {WeatherResponseInterface} from '../types/weatherResponse.interface';
import {stringify} from 'querystring';
import {GetWeatherRequestInterface} from '../types/getWeatherRequest.interface';

@Injectable()
export class WeatherService {
  constructor(private http: HttpClient) {
  }

  getWeather(request: GetWeatherRequestInterface): Observable<WeatherResponseInterface> {
    const urlParams = stringify({
      lat: request.lat,
      lon: request.lon,
      units: request.units,
      lang: 'ru',
      appid: environment.weatherApiKey
    });
    const fullUrl = `${environment.weatherApiUrl}?${urlParams}`;

    return this.http.get<WeatherResponseInterface>(fullUrl);
  }
}

import {WeatherStateInterface} from '../../weather/types/weatherState.interface';
import {CityStateInterface} from '../../city/types/cityState.interface';

export interface AppStateInterface {
  weather: WeatherStateInterface,
  city: CityStateInterface
}

export interface CityResponseInterface {
  features: CityFeatureInterface[]
}

interface CityPropertiesInterface {
  city: string,
  country: string,
  lon: number,
  lat: number,
}

export interface CityFeatureInterface {
  properties: CityPropertiesInterface
}

export interface CityCoordsInterface {
  latitude: number
  longitude: number
}

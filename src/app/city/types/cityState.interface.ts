import {CityCoordsInterface} from './cityCoordsInterface';
import {CityResponseInterface} from './cityResponse.interface';

export interface CityStateInterface {
  isLoading: boolean,
  cityCoords: CityCoordsInterface | null;
  cities: CityResponseInterface | null
}

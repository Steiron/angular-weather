import {createAction, props} from '@ngrx/store';
import {ActionType} from '../actionTypes';
import {CityResponseInterface} from '../../types/cityResponse.interface';

export const findCityByNameAction = createAction(
  ActionType.FIND_CITY,
  props<{name: string}>()
);
export const findCityByNameSuccessAction = createAction(
  ActionType.FIND_CITY_SUCCESS,
  props<{cities: CityResponseInterface}>()
);
export const findCityByNameFailureAction = createAction(
  ActionType.FIND_CITY_FAILURE
);

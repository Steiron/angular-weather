import {createAction, props} from '@ngrx/store';
import {ActionType} from '../actionTypes';
import {CityCoordsInterface} from '../../types/cityCoordsInterface';

export const getCurrentPositionAction = createAction(
  ActionType.GET_CURRENT_POSITION
);
export const getCurrentPositionSuccessAction = createAction(
  ActionType.GET_CURRENT_POSITION_SUCCESS,
  props<{currentPosition: CityCoordsInterface}>()
);
export const getCurrentPositionFailureAction = createAction(
  ActionType.GET_CURRENT_POSITION_FAILURE
);

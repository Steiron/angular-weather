import {createAction} from '@ngrx/store';
import {ActionType} from '../actionTypes';

export const applyFindCityAction = createAction(
  ActionType.APPLY_FIND_CITY
);


import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {CityService} from '../../services/city.service';
import {
  findCityByNameAction,
  findCityByNameFailureAction,
  findCityByNameSuccessAction
} from '../actions/findCityByName.action';
import {CityResponseInterface} from '../../types/cityResponse.interface';

@Injectable()
export class FindCityByNameEffect {
  constructor(private actions$: Actions, private cityService: CityService) {}

  findCityByNameEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(findCityByNameAction),
      switchMap(({name}) => {
        return this.cityService.findCityByName(name).pipe(
          map((cities: CityResponseInterface) => {
            return findCityByNameSuccessAction({cities: cities});
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(findCityByNameFailureAction());
          })
        );
      })
    )
  );
}

import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';

import {
  getCurrentPositionAction,
  getCurrentPositionFailureAction,
  getCurrentPositionSuccessAction
} from '../actions/getCurrentPosition.action';
import {CityService} from '../../services/city.service';
import {CityCoordsInterface} from '../../types/cityCoordsInterface';

@Injectable()
export class GetCurrentPositionEffect {
  constructor(private actions$: Actions, private cityService: CityService) {}

  getCurrentPosition$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCurrentPositionAction),
      switchMap(() => {
        return this.cityService.getCurrentPosition().pipe(
          map((cityCoords: CityCoordsInterface) => {
            return getCurrentPositionSuccessAction({
              currentPosition: cityCoords,
            });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(getCurrentPositionFailureAction());
          })
        );
      })
    )
  );
}

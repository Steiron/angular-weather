import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStateInterface} from '../../shared/types/appState.interface';
import {CityStateInterface} from '../types/cityState.interface';

export const cityFeatureSelector = createFeatureSelector<AppStateInterface,
  CityStateInterface>('city');

export const isLoadingSelector = createSelector(
  cityFeatureSelector,
  (cityState: CityStateInterface) => cityState.isLoading
);
export const cityCoordsSelector = createSelector(
  cityFeatureSelector,
  (cityState: CityStateInterface) => cityState.cityCoords
);

export const findCitiesSelector = createSelector(cityFeatureSelector,
  (cityState: CityStateInterface) => cityState.cities);

import {Action, createReducer, on} from '@ngrx/store';
import {
  getCurrentPositionAction,
  getCurrentPositionFailureAction,
  getCurrentPositionSuccessAction
} from './actions/getCurrentPosition.action';
import {CityStateInterface} from '../types/cityState.interface';
import {
  findCityByNameAction,
  findCityByNameFailureAction,
  findCityByNameSuccessAction
} from './actions/findCityByName.action';
import {applyFindCityAction} from './actions/applyFindCity.action';

const initialState: CityStateInterface = {
  isLoading: false,
  cities: null,
  cityCoords: {
    latitude: 55.7522,
    longitude: 37.6156
  }
};

const cityReducer = createReducer(
  initialState,
  on(
    getCurrentPositionAction,
    (state): CityStateInterface => ({
      ...state,
      isLoading: true
    })
  ),
  on(
    getCurrentPositionSuccessAction,
    (state, action): CityStateInterface => ({
      ...state,
      isLoading: false,
      cityCoords: action.currentPosition
    })
  ),
  on(
    getCurrentPositionFailureAction,
    (state): CityStateInterface => ({
      ...state,
      isLoading: false
    })
  ),
  on(
    findCityByNameAction,
    (state): CityStateInterface => ({
      ...state,
      isLoading: true
    })
  ),
  on(
    findCityByNameSuccessAction,
    (state, action): CityStateInterface => ({
      ...state,
      isLoading: false,
      cities: action.cities
    })
  ),
  on(
    findCityByNameFailureAction,
    (state): CityStateInterface => ({
      ...state,
      isLoading: false
    })
  ),
  on(
    applyFindCityAction,
    (state): CityStateInterface => ({
      ...state,
      cities: null
    })
  )
);

export function reducer(state: CityStateInterface, action: Action) {
  return cityReducer(state, action);
}

export enum ActionType {
  GET_CURRENT_POSITION = '[Get Current Position] Get Current Position',
  GET_CURRENT_POSITION_SUCCESS = '[Get Current Position] Get Current Position success',
  GET_CURRENT_POSITION_FAILURE = '[Get Current Position] Get Current Position failure',

  FIND_CITY = '[Find City] Find City',
  FIND_CITY_SUCCESS = '[Find City] Find City success',
  FIND_CITY_FAILURE = '[Find City] Find City failure',

  APPLY_FIND_CITY = '[Apply Find City] Apply Find City',
}

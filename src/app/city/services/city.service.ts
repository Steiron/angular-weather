import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {stringify} from 'querystring';

import {environment} from '../../../environments/environment';
import {CityCoordsInterface} from '../types/cityCoordsInterface';
import {CityResponseInterface} from '../types/cityResponse.interface';


@Injectable()
export class CityService {
  constructor(private http: HttpClient) {
  }


  findCityByName(name: string): Observable<CityResponseInterface> {
    const urlParams = stringify({
      text: name,
      type: 'city',
      limit: 5,
      lang: 'ru',
      apiKey: environment.cityApiKey
    });
    const fullUrl = `${environment.cityApiUrl}?${urlParams}`;

    return this.http.get<CityResponseInterface>(fullUrl);
  }

  getCurrentPosition(): Observable<CityCoordsInterface> {
    return new Observable((subscriber) => {
      if (navigator && navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(
          (position) => {
            const currentPosition: CityCoordsInterface = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            };
            subscriber.next(currentPosition);
            subscriber.complete();
          },
          (error) => subscriber.error(null)
        );
      } else {
        subscriber.error(null);
      }
    });
  }
}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import {findCitiesSelector} from '../../store/selectors';
import {CityFeatureInterface, CityResponseInterface} from '../../types/cityResponse.interface';
import {findCityByNameAction} from '../../store/actions/findCityByName.action';
import {CityCoordsInterface} from '../../types/cityCoordsInterface';
import {getCurrentPositionSuccessAction} from '../../store/actions/getCurrentPosition.action';
import {applyFindCityAction} from '../../store/actions/applyFindCity.action';

@Component({
  selector: 'app-city-finder',
  templateUrl: './cityFinder.component.html',
  styleUrls: ['./cityFinder.component.scss']
})
export class CityFinderComponent implements OnInit {

  @Output('onApplyCity') onApplyCityEvent = new EventEmitter<void>();

  cityName: string;
  selectedFindCity: CityFeatureInterface | null;
  findCities$: Observable<CityResponseInterface>;

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    this.initializeValues();
  }

  private initializeValues(): void {
    this.findCities$ = this.store.pipe(select(findCitiesSelector));
  }

  onCityNameChange(): void {
    if (!this.cityName) {
      return;
    }
    let city = this.cityName.trim();
    if (!city) {
      return;
    }
    city = city[0].toUpperCase() + city.substr(1).toLowerCase();
    this.store.dispatch(findCityByNameAction({name: city}));
    this.selectedFindCity = null;
  }

  onCityClick(feature: CityFeatureInterface): void {
    this.cityName = feature.properties.city;
    this.selectedFindCity = feature;
  }

  onClearInput(): void {
    this.cityName = '';
  }

  joinCityName(cityFeature: CityFeatureInterface): string {
    const {city, country} = cityFeature.properties;

    return `${city}, ${country}`;
  }

  onApplyCity(): void {
    const cityCoords: CityCoordsInterface = {
      longitude: this.selectedFindCity.properties.lon,
      latitude: this.selectedFindCity.properties.lat
    };

    this.store.dispatch(getCurrentPositionSuccessAction({currentPosition: cityCoords}));
    this.store.dispatch(applyFindCityAction());
    this.onApplyCityEvent.emit();
  }
}

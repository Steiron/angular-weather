import {Component, Input, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {CityCoordsInterface} from '../../types/cityCoordsInterface';
import {getCurrentPositionAction} from '../../store/actions/getCurrentPosition.action';
import {cityCoordsSelector} from '../../store/selectors';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {

  @Input('cityName') cityNameProps: string;
  currentPosition$: Observable<CityCoordsInterface>;
  isChangeCity: boolean = false;

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    this.initializeValues();
  }

  private initializeValues(): void {
    this.currentPosition$ = this.store.pipe(select(cityCoordsSelector));
  }

  getCurrentPosition(): void {
    this.store.dispatch(getCurrentPositionAction());
  }

  onChangeCity() {
    this.isChangeCity = !this.isChangeCity;
  }
}

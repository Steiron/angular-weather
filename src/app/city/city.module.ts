import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {FormsModule} from '@angular/forms';

import {CityComponent} from './components/city/city.component';
import {CityFinderComponent} from './components/cityFinder/cityFinder.component';
import {CityService} from './services/city.service';

import {GetCurrentPositionEffect} from './store/effects/getCurrentPosition.effect';
import {reducer} from './store/reducer';
import {FindCityByNameEffect} from './store/effects/findCityByName.effect';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('city', reducer),
    EffectsModule.forFeature([GetCurrentPositionEffect, FindCityByNameEffect]),
    FormsModule,
  ],
  declarations: [CityComponent, CityFinderComponent],
  providers: [CityService],
  exports: [CityComponent],
})
export class CityModule {}
